// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = {
  parser: 'sugarss',
  plugins: {
    'postcss-easy-import': {
      plugins: [ require('stylelint')({}) ], // eslint-disable-line
    },
    'postcss-normalize': {},
    precss: {},
    'postcss-nested-props': {},
    'postcss-pxtorem': {
      propList: ['*', '!font*']
    },
    'postcss-hexrgba': {},
    // to edit target browsers: use "browserslist" field in package.json
    autoprefixer: {},
  },
};
