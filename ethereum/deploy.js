const fs = require('fs-extra');
const os = require('os');
const path = require('path');
const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledFactory = require('./build/CampaignFactory.json');

const provider = new HDWalletProvider(
  'tennis license slight test surround upgrade forum route spawn people elder simple',
  'https://rinkeby.infura.io/v3/544e52e9dbe540c0bbde17fa3bf80a77',
  1,
);

const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts();

  console.log('Attempting to deploy from account', accounts[0]);

  try {
    const factory = await new web3.eth.Contract(
      JSON.parse(compiledFactory.interface),
    )
      .deploy({ data: compiledFactory.bytecode })
      .send({ gas: '2000000', from: accounts[0] });

    const factoryAddress = `export default '${factory.options.address}';${os.EOL}`;

    fs.outputFileSync(
      path.resolve(__dirname, 'factoryAddress.js'),
      factoryAddress,
    );

    console.log('CampaignFactory deployed to', factory.options.address);
  } catch (error) {
    console.log(error);
  }
};

deploy();
