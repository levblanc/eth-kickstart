import Web3 from 'web3';

let provider;

if (typeof window !== 'undefined' && typeof window.web3 !== 'undefined') {
  // we are in the browser and MetaMask is running
  provider = window.web3.currentProvider;
} else {
  // we are on the server *OR* the user is not running MetaMask
  provider = new Web3.providers.HttpProvider(
    'https://rinkeby.infura.io/v3/544e52e9dbe540c0bbde17fa3bf80a77',
  );
}

const web3 = new Web3(provider);

export default web3;
