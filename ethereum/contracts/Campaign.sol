pragma solidity ^0.4.17;

contract CampaignFactory {
    address[] public deployedCampaigns;
    event CampaignCreated(address newCampaignAddress);

    function createCampaign(
        string campaignName,
        string campaignDesc,
        uint minimun
    ) public {
        address newCampaign = new Campaign(
          campaignName,
          campaignDesc,
          minimun,
          msg.sender
        );

        deployedCampaigns.push(newCampaign);
        emit CampaignCreated(newCampaign);
    }

    function getDeployedCampaigns() public view returns(address[]) {
        return deployedCampaigns;
    }
}

contract Campaign {
    struct Request {
        string description;
        uint value;
        address recipient;
        bool complete;
        uint approvalCount;
        mapping(address => bool) approvals;
    }

    Request[] public requests;
    address public manager;
    string public campaignName;
    string public campaignDesc;
    uint public minimumContribution;
    mapping(address => bool) public approvers;
    uint public approversCount;

    modifier restricted() {
        require(msg.sender == manager);
        _;
    }

    constructor(string name, string desc, uint minimum, address creator) public {
        manager = creator;
        campaignName = name;
        campaignDesc = desc;
        minimumContribution = minimum;
    }

    function contribute() public payable {
        require(!approvers[msg.sender], "You've already contributed to this campaign.");
        require(msg.value > minimumContribution, "Your contribute value is less than minimum contribution required.");

        approvers[msg.sender] = true;
        approversCount++;
    }

    function createRequest(string description, uint value, address recipient) public restricted {
        Request memory newRequest = Request({
            description: description,
            value: value,
            recipient: recipient,
            complete: false,
            approvalCount: 0
        });

        requests.push(newRequest);
    }

    function approveRequest(uint index) public {
        Request storage targetRequest = requests[index];

        require(approvers[msg.sender]);
        require(!targetRequest.approvals[msg.sender]);

        targetRequest.approvals[msg.sender] = true;
        targetRequest.approvalCount++;
    }

    function finalizeRequest(uint index) public restricted {
        Request storage targetRequest = requests[index];

        require(!targetRequest.complete);
        require(targetRequest.approvalCount > (approversCount / 2));

        targetRequest.recipient.transfer(targetRequest.value);
        targetRequest.complete = true;
    }

    function getSummary() public view returns(
      uint, uint, uint, uint, address, string, string
    ) {
        return (
            minimumContribution,
            address(this).balance,
            requests.length,
            approversCount,
            manager,
            campaignName,
            campaignDesc
        );
    }

    function getRequestsCount() public view returns(uint) {
        return requests.length;
    }
}
