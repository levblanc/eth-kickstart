const express = require('express');
const util    = require('util');
const fs      = require('fs');
const path    = require('path');
const _       = require('lodash');

const config = require('./config');
const routerPaths = require('./src/router/paths.json');

const { index, assetsPublicPath, assetsSubDirectory } = config.build;
const readFile = util.promisify(fs.readFile);
const app = express();
const port = process.env.PORT || 9876;

const pageUrls = _.values(routerPaths);

const renderPage = async (req, res) => {
  const html = await readFile(path.resolve(index), 'utf8');
  const configs = { ENV: process.env.NODE_ENV || 'alpha' };
  const configStr = `var CONFIG = ${JSON.stringify(configs)};`;
  // replace configs
  const htmlWithConfigs = html.replace(/var CONFIG.*;/, configStr);
  res.send(htmlWithConfigs);
};

const staticRoot = assetsPublicPath + assetsSubDirectory;
const staticDirPath = path.resolve(__dirname, `./dist/${assetsSubDirectory}`);

app.use(staticRoot, express.static(staticDirPath, {
  setHeaders: (res) => {
    res.set('Cache-Control', 'public, max-age=31536000, s-maxage=3600');
  },
}));

// 处理txt文件读取
app.get('*.txt', (req, res) => {
  const files = fs.readdirSync(staticDirPath);
  let targetTxt;

  for (let i = 0; i < files.length; i++) { // eslint-disable-line
    // 必须保证static文件夹下只有一个txt文件
    // 否则sendFile会发送最后一个匹配的txt文件
    // 导致读取错的文件
    if (files[i].indexOf('.txt') !== -1) {
      targetTxt = files[i];
    }
  }

  if (!targetTxt) return;

  res.sendFile(targetTxt, {
    root: staticDirPath,
    headers: {
      'Content-Type': 'text/plain',
    },
  });
});

_.each(pageUrls, (url) => {
  app.get(url, renderPage);
});

app.listen(port);

console.info(`> Listening at http://127.0.0.1:${port}/`);
