## Vue Boilerplate

Vue 2 + webpack 3 开发框架。

### 环境配置要求

|      | version |
|------|---------|
| node | v8.0.0+ |
| npm  | v5.0.0+ |

### 使用到的技术
- [pug](https://pugjs.org/api/getting-started.html) (html模板)
- [postcss](https://github.com/postcss/postcss#usage) ([sugarSS](https://github.com/postcss/sugarss)语法)
- ES6
- vue
- vuex
- vue-router

### 第三方库
- [lodash](https://lodash.com/docs)
- [axios](https://github.com/axios/axios)
- [query-string](https://www.npmjs.com/package/query-string)

### 安装依赖

```bash
$ npm install
```

### 项目运行、打包和测试的命令

```bash
# 以 alpha 环境api进行开发
$ npm run dev

# 以 beta 环境api进行开发
$ npm run dev:beta

# 以 release 环境api进行开发
$ npm run dev:release

# 以 alpha 环境api进行打包
$ npm run build

# 以 beta 环境api进行打包
$ npm run build:beta

# 以 release 环境api进行打包
$ npm run build:release

# 打包并查看 bundle analyzer 的分析报告
$ npm run build --report

# 运行测试
$ npm test
```

### 代码提交规则

项目中使用了工具对git提交时的commit message进行检测，必须按照规则书写，否则**无法提交代码**。

规则请参考[这篇文章](http://www.ruanyifeng.com/blog/2016/01/commit_message_change_log.html)。提交时，参照2.1 Header部分为每个提交信息增加commit的类别，也可参考过往的已提交的信息。


### 项目结构

```bash
.
├── build             # 项目打包配置文件，包括 webpack config
├── config            # 项目环境变量配置
├── dist              # 最后生成的build
└── src               # 源文件
│   ├── assets        # 静态资源文件images、全局的styles等
│   ├── components    # 自定义的组件
│   ├── http          # ajax封装
│   ├── router        # vue router
│   ├── store         # vuex store
│   ├── views         # 项目页面
│   ├── utils         # 根据项目需要而编写的辅助工具 (根据自身需要增加)
│   ├── App.vue       # 应用主 template
│   ├── constants.js  # 常量配置
│   └── main.js       # Vue 应用入口文件，boostrap并初始化整个应用
├── static            # 静态资源文件
├── index.pug         # 项目入口文件
├── server.js         # 简单的服务器，部署时启动本文件
├── package-lock.json # 依赖包安装记录文件，保证合作开发者之间依赖包版本一致
├── package.json      # 包含项目信息、项目依赖包及npm运行命令
├── .babelrc          # babel 配置文件
├── .editorconfig     # editorconfig 配置文件
├── .eslintrc.js      # eslint 配置文件
├── .postcssrc.js     # postcss 配置文件
├── .stylelintrc      # stylelint 配置文件
└── README.md         # 项目描述
```
