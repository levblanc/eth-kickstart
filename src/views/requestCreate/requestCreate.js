import container from '@/components/Container';
import { mapState, mapActions } from 'vuex';
import errorMessage from '@/components/ErrorMessage';
import warningMessage from '@/components/WarningMessage';

export default {
  components: {
    container,
    errorMessage,
    warningMessage,
  },
  data() {
    return {
      address       : '',
      description   : '',
      transferAmount: '',
      recipient     : '',
      disableFormSubmit: true,
    };
  },
  computed: {
    ...mapState({
      loading        : ({ requestCreate }) => requestCreate.loading,
      createSuccess  : ({ requestCreate }) => requestCreate.success,
      errorMsg       : ({ requestCreate }) => requestCreate.error,
      campaignBalance: ({ campaignBalance }) => campaignBalance.balance,
      lang           : ({ language }) => language.requestCreate,
      globalLang     : ({ language }) => language.global,
    }),
  },
  methods: {
    ...mapActions([
      'getCampaignBalance',
      'requestCreate',
      'modifyNewRequestError',
    ]),
    onAmountInput(e) {
      const { value } = e.target;

      this.transferAmount = value;

      if (!value) {
        this.modifyNewRequestError(this.lang.requestAmountEmpty);
        this.disableFormSubmit = true;
        return;
      }

      const valueNum = parseFloat(value, 10);

      if (isNaN(valueNum) || valueNum <= 0) {
        this.modifyNewRequestError(this.globalLang.positiveIntWithDecimal);
        this.disableFormSubmit = true;
        return;
      }

      if (valueNum > parseFloat(this.campaignBalance, 10)) {
        this.modifyNewRequestError(`${this.lang.requestAmountTooLarge} ${this.campaignBalance} ether`);
        this.disableFormSubmit = true;
        return;
      }

      this.modifyNewRequestError('');
      this.disableFormSubmit = false;
    },
    async createRequest(e) {
      e.preventDefault();

      this.modifyNewRequestError('');

      await this.requestCreate({
        address    : this.address,
        description: this.description,
        value      : this.transferAmount,
        recipient  : this.recipient,
      });

      if (this.errorMsg) return;

      this.$router.back();
    },
    backToRequestList(e) {
      e.preventDefault();

      this.$router.push({
        name: 'requestList',
        params: {
          address: this.address,
        },
      });
    },
  },
  async created() {
    const { address } = this.$route.params;
    this.address = address;

    if (this.errorMsg) {
      this.modifyNewRequestError('');
    }

    await this.getCampaignBalance(address);
  },
};
