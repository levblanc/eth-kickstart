import { mapState, mapActions } from 'vuex';
import container from '@/components/Container';
import errorMessage from '@/components/ErrorMessage';
import warningMessage from '@/components/WarningMessage';

export default {
  components: {
    container,
    errorMessage,
    warningMessage,
  },
  data() {
    return {
      campaignName: '',
      campaignDesc: '',
      minimumContribution: '',
      disableFormSubmit: true,
      showModal: false,
    };
  },
  computed: {
    ...mapState({
      loading   : ({ campaignCreate }) => campaignCreate.loading,
      newAddress: ({ campaignCreate }) => campaignCreate.address,
      errorMsg  : ({ campaignCreate }) => campaignCreate.error,
      lang      : ({ language }) => language.newCampaign,
      globalLang: ({ language }) => language.global,
    }),
  },
  methods: {
    ...mapActions([
      'campaignCreate',
      'modifyNewCampaignError',
    ]),
    onContributionInput(e) {
      const { value } = e.target;

      this.minimumContribution = value;

      if (!value) {
        this.modifyNewCampaignError(this.lang.minContributionEmpty);
        this.disableFormSubmit = true;
        return;
      }

      const valueNum = parseFloat(value, 10);

      if (isNaN(valueNum) || valueNum <= 0) {
        this.modifyNewCampaignError(this.globalLang.positiveIntWithDecimal);
        this.disableFormSubmit = true;
        return;
      }

      this.modifyNewCampaignError('');
      this.disableFormSubmit = false;
    },
    async createCampaign(e) {
      e.preventDefault();

      if (!this.campaignName) {
        this.modifyNewCampaignError(this.lang.campaignNamePlaceholder);
        return;
      }

      if (!this.campaignDesc) {
        this.modifyNewCampaignError(this.lang.campaignDescPlaceholder);
        return;
      }

      this.modifyNewCampaignError('');

      await this.campaignCreate({
        campaignName       : this.campaignName,
        campaignDesc       : this.campaignDesc,
        minimumContribution: this.minimumContribution,
      });

      if (this.errorMsg) return;

      this.showModal = true;
    },
    copyConfirmed() {
      this.showModal = false;
      this.$router.push({
        name: 'home',
      });
    },
  },
  created() {
    if (this.errorMsg) {
      this.modifyNewCampaignError('');
    }
  },
};
