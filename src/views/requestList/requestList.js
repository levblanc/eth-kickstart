import { mapState, mapActions } from 'vuex';
import container from '@/components/Container';
import requestTableRow from '@/components/RequestTableRow';
import errorMessage from '@/components/ErrorMessage';
import warningMessage from '@/components/WarningMessage';

export default {
  components: {
    container,
    requestTableRow,
    errorMessage,
    warningMessage,
  },
  data() {
    return {
      address: '',
    };
  },
  computed: {
    ...mapState({
      loading        : ({ requestList }) => requestList.loading,
      requestList    : ({ requestList }) => requestList.list,
      requestCount   : ({ requestList }) => requestList.count,
      totalApprovers : ({ requestList }) => requestList.totalApprovers,
      approveLoading : ({ requestApprove }) => requestApprove.loading,
      approveError   : ({ requestApprove }) => requestApprove.error,
      finalizeLoading: ({ requestFinalize }) => requestFinalize.loading,
      finalizeError  : ({ requestFinalize }) => requestFinalize.error,
      lang           : ({ language }) => language.requestList,
      isManager      : ({ campaignManager }) => campaignManager.isManager,
    }),
  },
  methods: {
    ...mapActions([
      'getRequestList',
      'getCampaignManager',
    ]),
    addRequest() {
      this.$router.push({
        name: 'requestCreate',
        params: {
          address: this.address,
        },
      });
    },
    goToCampaignSummary() {
      this.$router.push({
        name: 'campaignDetail',
        params: {
          address: this.address,
        },
      });
    },
  },
  async mounted() {
    const { address } = this.$route.params;
    this.address = address;

    await this.getCampaignManager(address);
    await this.getRequestList(address);
  },
};
