import { mapState, mapActions } from 'vuex';
import container from '@/components/Container';

export default {
  components: {
    container,
  },
  data() {
    return {};
  },
  computed: {
    ...mapState({
      loading     : ({ campaigns }) => campaigns.loading,
      campaignList: ({ campaigns }) => campaigns.list,
      error       : ({ campaigns }) => campaigns.error,
      lang        : ({ language }) => language.home,
    }),
  },
  methods: {
    ...mapActions([
      'getCampaignList',
    ]),
  },
  async mounted() {
    await this.getCampaignList();
  },
};
