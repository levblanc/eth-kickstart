import { mapActions, mapState } from 'vuex';
import container from '@/components/Container';
import contributeForm from '@/components/ContributeForm';

export default {
  components: {
    container,
    contributeForm,
  },
  data() {
    return {
      address: '',
    };
  },
  computed: {
    ...mapState({
      loading            : ({ campaignDetail }) => campaignDetail.loading,
      campaignSummary    : ({ campaignDetail }) => campaignDetail.summary,
      minimumContribution: ({ campaignDetail }) => campaignDetail.minimumContribution,
      lang               : ({ language }) => language.campaignDetail,
    }),
  },
  methods: {
    ...mapActions([
      'getCampaignSummary',
    ]),
    goToRequestList() {
      this.$router.push({
        name: 'requestList',
        params: {
          address: this.address,
        },
      });
    },
  },
  async created() {
    const { address } = this.$route.params;
    this.address = address;

    await this.getCampaignSummary(address);
  },
};
