import web3 from '$eth/web3';
import { mapActions, mapState } from 'vuex';

export default {
  props: {
    campaignAddress: {
      type: String,
      required: true,
    },
    rowItem: {
      type: Object,
      required: true,
    },
    requestIndex: {
      type: Number,
      required: true,
    },
    totalApprovers: {
      type: Number,
      required: true,
    },
  },
  data() {
    return {
      fromWei: web3.utils.fromWei,
    };
  },
  computed: {
    ...mapState({
      approveLoading : ({ requestApprove }) => requestApprove.loading,
      approveError   : ({ requestApprove }) => requestApprove.error,
      finalizeLoading: ({ requestFinalize }) => requestFinalize.loading,
      finalizeError  : ({ requestFinalize }) => requestFinalize.error,
      lang           : ({ language }) => language.requestList,
      isManager      : ({ campaignManager }) => campaignManager.isManager,
    }),
  },
  methods: {
    ...mapActions([
      'requestApprove',
      'requestFinalize',
      'getRequestList',
      'modifyApproveError',
      'modifyFinalizeError',
    ]),
    async approveRequest() {
      this.modifyApproveError('');

      await this.requestApprove({
        address: this.campaignAddress,
        index  : this.requestIndex,
      });

      if (this.approveError) return;

      await this.getRequestList(this.campaignAddress);
    },
    async finalizeRequest() {
      this.modifyFinalizeError('');

      await this.requestFinalize({
        address: this.campaignAddress,
        index  : this.requestIndex,
      });

      if (this.finalizeError) return;

      await this.getRequestList(this.campaignAddress);
    },
  },
};
