import { mapActions, mapState } from 'vuex';

export default {
  data() {
    return {
    };
  },
  computed: {
    ...mapState({
      langSetting: ({ language }) => language.setting.toUpperCase(),
      lang       : ({ language }) => language.header,
    }),
  },
  methods: {
    ...mapActions([
      'changeLanguage',
    ]),
    goToHomePage() {
      this.$router.push({
        name: 'home',
      });
    },
    goToNewCampaign() {
      this.$router.push({
        name: 'newCampaign',
      });
    },
  },
};
