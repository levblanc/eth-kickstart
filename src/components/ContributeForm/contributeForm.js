import { mapActions, mapState } from 'vuex';
import errorMessage from '@/components/ErrorMessage';
import warningMessage from '@/components/WarningMessage';

export default {
  props: {
    campaignAddress: {
      type: String,
      required: true,
    },
    minimumContribution: {
      type: [Number, String],
      required: true,
    },
  },
  components: {
    errorMessage,
    warningMessage,
  },
  data() {
    return {
      contribution: '',
      disableFormSubmit: false,
    };
  },
  computed: {
    ...mapState({
      loading          : ({ campaignContribute }) => campaignContribute.loading,
      contributeSuccess: ({ campaignContribute }) => campaignContribute.success,
      errorMsg         : ({ campaignContribute }) => campaignContribute.error,
      lang             : ({ language }) => language.contributeForm,
      globalLang       : ({ language }) => language.global,
    }),
  },
  methods: {
    ...mapActions([
      'getCampaignSummary',
      'campaignContribute',
      'modifyContributeError',
    ]),
    onContributionInput(e) {
      const { value } = e.target;

      this.contribution = value;

      if (!value) {
        this.modifyContributeError(this.globalLang.contributeAmountEmpty);
        this.disableFormSubmit = true;
        return;
      }

      const valueNum = parseFloat(value, 10);

      if (isNaN(valueNum) || valueNum <= 0) {
        this.modifyContributeError(this.globalLang.positiveIntWithDecimal);
        this.disableFormSubmit = true;
        return;
      }

      if (valueNum <= this.minimumContribution) {
        this.modifyContributeError(`${this.lang.contributeAmountTooLittle} ${this.minimumContribution}`);
        return;
      }

      this.modifyContributeError('');
      this.disableFormSubmit = false;
    },
    async contributeToCampaign(e) {
      e.preventDefault();

      this.modifyContributeError('');

      await this.campaignContribute({
        address     : this.campaignAddress,
        contribution: this.contribution,
      });

      if (this.errorMsg) return;

      this.contribution = '';

      await this.getCampaignSummary(this.campaignAddress);
    },
  },
  created() {
    if (this.errorMsg) {
      this.modifyContributeError('');
    }
  },
};
