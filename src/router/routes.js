import pageTitle from '@/lang/pageTitle';
import paths from './paths.json';

/**
 * 获取页面title
 * @param {string} page 页面名称
 * @return {object}     页面title键值对
 */
const title = page => ({ title: pageTitle[page] });

/**
 * 生成meta对象
 * @param {string} page 页面名称
 * @return {object}     meta对象
 */
const meta  = page => Object.assign({}, title(page));

/**
 * 获取页面路由
 * @param {string} name 页面名称
 * @return {string}     页面路由
 */
const path  = name => paths[name];

/**
 * 根据页面名称引入对应文件夹下的vue component
 * @param {string} name 页面名称
 */
const view  = name => () => import(`@/views/${name}/index.vue`);

export default [
  {
    name     : 'pageNotFound',
    meta     : meta('pageNotFound'),
    path     : path('pageNotFound'),
    component: view('pageNotFound'),
  },
  {
    name     : 'home',
    meta     : meta('home'),
    path     : path('home'),
    component: view('home'),
  },
  {
    name     : 'newCampaign',
    meta     : meta('newCampaign'),
    path     : path('newCampaign'),
    component: view('newCampaign'),
  },
  {
    name     : 'campaignDetail',
    meta     : meta('campaignDetail'),
    path     : path('campaignDetail'),
    component: view('campaignDetail'),
  },
  {
    name     : 'requestList',
    meta     : meta('requestList'),
    path     : path('requestList'),
    component: view('requestList'),
  },
  {
    name     : 'requestCreate',
    meta     : meta('requestCreate'),
    path     : path('requestCreate'),
    component: view('requestCreate'),
  },
];
