export default (router) => {
  router.beforeEach((to, from, next) => {
    const { title } = to.meta;
    // 设置document title
    document.title = title;

    next();
  });
};
