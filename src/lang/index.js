/**
 * 通过 `@/lang/index.js` 一次性引入所有语言文字
 */

const files = require.context('.', false, /\.js$/);
const lang = {};

files.keys().forEach((key) => {
  if (key === './index.js') return;

  const vuexObj = files(key);

  lang[key.replace(/(\.\/|\.js)/g, '')] = vuexObj.default;
});

export default lang;
