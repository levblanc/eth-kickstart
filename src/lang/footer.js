export default {
  cn: {
    howTo       : '怎么玩？',
    errorsAndFaq: '常见报错及FAQ',
    more        : '更多',

  },
  en: {
    howTo       : 'How to Use?',
    errorsAndFaq: 'Errors & FAQ',
    more        : 'More',
  },
};
