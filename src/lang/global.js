export default {
  cn: {
    topBanner: '注意：本站使用的是 Rinkeby 测试网络（Rinkeby Test Network）的数据，并不使用主网（Main Ethereum Network）的数据。',
    transactionTime: '目前以太坊网络单个操作需时 15 - 30 秒，根据不同的网络情况，等待时间有可能延长，请耐心等候',
    positiveIntWithDecimal: '输入正数，可包含小数点',
  },
  en: {
    topBanner: 'Note: this website uses data on Rinkeby Test Network, not Main Ethereum Network',
    transactionTime: 'Transaction on the Ethereum network takes 15 - 30 seconds, and may take longer under different network conditions, please wait',
    positiveIntWithDecimal: 'Please type in positive interger, or positive number with decimal',
  },
};
