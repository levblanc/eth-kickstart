export default {
  cn: {
    siteName    : 'CrowdCoin',
    campaignList: '项目列表',
    newCampaign : '新建项目',
  },
  en: {
    siteName    : 'CrowdCoin',
    campaignList: 'Campaigns List',
    newCampaign : 'New Campaign',
  },
};
