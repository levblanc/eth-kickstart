export default {
  cn: {
    formTitle: '支持本项目',
    inputPlaceholder: '请输入支持的金额',
    confirm: '确定',
    contributeAmountEmpty: '请输入支持的金额',
    contributeAmountTooLittle: '支持的金额需大于：',
  },
  en: {
    formTitle: 'Contribute to This Campaign!',
    inputPlaceholder: 'amount of contribution in ether',
    confirm: 'Contribute!',
    contributeAmountEmpty: 'Please specify your contribute amount',
    contributeAmountTooLittle: 'Contribute amount should be larger than ',
  },
};
