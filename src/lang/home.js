export default {
  cn: {
    openCampaigns    : '项目列表',
    viewCampaign     : '查看项目',
    viewOnEtherscan  : '在Etherscan查看',
    createCampaign   : '新建项目',
    noCampaignWarning: '暂无项目，马上创建一个吧！',
  },
  en: {
    openCampaigns    : 'Open Campaigns',
    viewCampaign     : 'View Campaign',
    viewOnEtherscan  : 'View on Etherscan',
    createCampaign   : 'Create Campaign',
    noCampaignWarning: 'No campaigns yet, create one now!',
  },
};
