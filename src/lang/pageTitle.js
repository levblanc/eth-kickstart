export default {
  pageNotFound  : '找不到页面',
  home          : 'Home',
  newCampaign   : 'New Campaign',
  campaignDetail: 'Campaign Detail',
  requestList   : 'Request List',
  requestCreate : 'Create Request',
};
