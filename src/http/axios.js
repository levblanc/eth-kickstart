import _ from 'lodash';
import axios from 'axios';
import queryString from 'query-string';
import {
  reqInterceptors, resInterceptors,
} from '@/http/interceptors';

const { interceptors } = axios;

/**
 * 判断是否post, put, patch方法
 * @param  {string} method  http方法名称
 * @return {boolean}        是否三个方法之一
 */
const updateMethods = (method) => {
  const regex = new RegExp(/post|put|patch/g);
  return regex.test(method);
};

// 自定义 axios 默认配置
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

// request interceptors
interceptors.request.use(reqInterceptors.resolveApiUrl);

// response interceptors
interceptors.response.use(
  resInterceptors.successHandler,
  resInterceptors.errorHandler,
);

const axiosRequest = httpMethod => (url, query = {}, options = {}) => {
  if (!url) {
    throw new Error('请传入参数url');
  } else if (typeof url !== 'string') {
    throw new Error('参数url的类型必须为string，请检查');
  }

  const isUpdateMethods = updateMethods(httpMethod);
  // request timeout(ms)
  const requestTimeout = 5 * 1000;
  const reqQuery = {};
  const config = {
    timeout: requestTimeout,
  };

  if (!isUpdateMethods) {
    _.extend(reqQuery, query);
  } else if (query.params) {
    _.extend(reqQuery, query.params);
  } else if (query.data) {
    config.data = queryString.stringify(query.data);
  } else {
    config.data = queryString.stringify(query);
  }

  config.method  = httpMethod;
  config.url     = url;
  config.params  = reqQuery;
  config.options = options;

  return axios(config);
};

const ajax = {
  get     : axiosRequest('get'),
  post    : axiosRequest('post'),
  head    : axiosRequest('head'),
  options : axiosRequest('options'),
  put     : axiosRequest('put'),
  patch   : axiosRequest('patch'),
  del     : axiosRequest('delete'),
};

export default ajax;
