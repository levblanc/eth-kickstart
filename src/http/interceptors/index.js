// 将 request、response 文件夹内，
// 除index.js以外的文件所export的default function
// 放到interceptors对象中，export出去
// https://webpack.github.io/docs/context.html#context-module-api

const indexer = (dir) => {
  let requireFiles;
  const interceptors = {};

  // require.context的第一个参数，不能为动态，唯有这样判断一下
  // https://webpack.js.org/guides/dependency-management/#require-context
  // The arguments passed to require.context must be literals!
  if (dir === 'request') {
    requireFiles = require.context('./request', false, /\.js$/);
  } else {
    requireFiles = require.context('./response', false, /\.js$/);
  }

  requireFiles.keys().forEach((file) => {
    const key = file.replace(/(\.\/|\.js)/g, '');
    interceptors[key] = requireFiles(file).default;
  });

  return interceptors;
};

export const reqInterceptors = indexer('request');
export const resInterceptors = indexer('response');
