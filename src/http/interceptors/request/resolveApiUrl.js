import _ from 'lodash';
import { API_HOST } from '@/constants';

export default (config) => {
  let { url } = config;
  const regex = new RegExp(/(http:\/\/|https:\/\/)/g);
  const isAbsoluteAddr = regex.test(url);

  // 传入的为绝对请求路径，直接返回
  if (isAbsoluteAddr) return config;

  if (_.head(url) !== '/') {
    throw new Error('url 参数必须以"/"开头，请检查');
  }

  const apiHost = API_HOST[CONFIG.ENV || CONFIG.API_ENV];

  if (!apiHost) {
    throw new Error('apiHost出错。请检查页面url中的env参数，或服务启动命令中NODE_ENV值。');
  }

  if (_.last(apiHost) === '/') {
    throw new Error('Api host 不需要以"/"结束，请检查');
  }

  // 拼接最终路径
  url = `${apiHost}${url}`;
  config.url = url;

  return config;
};
