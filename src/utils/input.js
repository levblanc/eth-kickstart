export const isUint = (num) => { // eslint-disable-line
  const regexp = new RegExp(/^[0-9]\d*$/g);
  return regexp.test(num);
};
