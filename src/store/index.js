import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';

import modules from './modules';

Vue.use(Vuex);

const isProduction = process.env.NODE_ENV === 'production';
const debug = !isProduction;

export default new Vuex.Store({
  ...modules,
  strict: debug,
  // store中使用的插件
  plugins: debug ? [createLogger()] : [],
});
