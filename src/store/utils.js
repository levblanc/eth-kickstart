export const setType = moduleName => mutationName => `${moduleName}/${mutationName}`; // eslint-disable-line

export const replaceParam = (apiUrl, param) => apiUrl.replace(/:\w+/g, param);
