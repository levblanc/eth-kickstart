import { setType } from '@/store/utils';
import {
  CAMPAIGN_CONTRIBUTE_REQUEST,
  CAMPAIGN_CONTRIBUTE_SUCCESS,
  CAMPAIGN_CONTRIBUTE_FAILURE,
  MODIFY_ERROR_MSG,
} from '@/store/mutationTypes';

import Campaign from '$eth/campaign';
import web3 from '$eth/web3';

const moduleName = 'campaignContribute';
const type = setType(moduleName);

const state = {
  loading: false,
  success: false,
  error  : '',
};

const mutations = {
  [type(CAMPAIGN_CONTRIBUTE_REQUEST)](state) {
    state.loading = true;
  },
  [type(CAMPAIGN_CONTRIBUTE_SUCCESS)](state, res) {
    state.loading = false;
    state.success = res;
    state.error   = '';
  },
  [type(CAMPAIGN_CONTRIBUTE_FAILURE)](state, err) {
    state.loading = false;
    state.success = false;
    state.error   = err;
  },
  [type(MODIFY_ERROR_MSG)](state, msg) {
    state.error = msg;
  },
};


const campaignContribute = async ({ commit }, { address, contribution }) => {
  commit(type(CAMPAIGN_CONTRIBUTE_REQUEST));

  const campaign = Campaign(address);

  try {
    const accounts = await web3.eth.getAccounts();
    const contributionValue = web3.utils.toWei(contribution, 'ether');

    const res = await campaign.methods.contribute()
      .send({
        from: accounts[0],
        value: contributionValue,
      });

    commit(type(CAMPAIGN_CONTRIBUTE_SUCCESS), res);
  } catch (error) {
    commit(type(CAMPAIGN_CONTRIBUTE_FAILURE), error.message);
  }
};

const modifyContributeError = ({ commit }, msg) => {
  commit(type(MODIFY_ERROR_MSG), msg);
};


export const actions = {
  campaignContribute,
  modifyContributeError,
};

export default {
  state,
  mutations,
};
