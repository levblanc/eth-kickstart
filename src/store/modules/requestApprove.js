import { setType } from '@/store/utils';
import {
  REQUEST_APPROVE_REQUEST,
  REQUEST_APPROVE_SUCCESS,
  REQUEST_APPROVE_FAILURE,
  MODIFY_ERROR_MSG,
} from '@/store/mutationTypes';
import Campaign from '$eth/campaign';
import web3 from '$eth/web3';

const moduleName = 'requestApprove';
const type = setType(moduleName);

const state = {
  loading: false,
  success: false,
  error  : '',
};

const mutations = {
  [type(REQUEST_APPROVE_REQUEST)](state) {
    state.loading = true;
  },
  [type(REQUEST_APPROVE_SUCCESS)](state, res) {
    state.loading = false;
    state.success = res;
    state.error   = '';
  },
  [type(REQUEST_APPROVE_FAILURE)](state, err) {
    state.loading = false;
    state.success = false;
    state.error   = err;
  },
  [type(MODIFY_ERROR_MSG)](state, msg) {
    state.error = msg;
  },
};


const requestApprove = async ({ commit }, { address, index }) => {
  commit(type(REQUEST_APPROVE_REQUEST));

  const campaign = Campaign(address);

  try {
    const accounts = await web3.eth.getAccounts();

    const res = await campaign.methods.approveRequest(index)
      .send({
        from: accounts[0],
        gas: '3000000',
      });

    commit(type(REQUEST_APPROVE_SUCCESS), res);
  } catch (err) {
    commit(type(REQUEST_APPROVE_FAILURE), err.message);
  }
};

const modifyApproveError = ({ commit }, msg) => {
  commit(type(MODIFY_ERROR_MSG), msg);
};


export const actions = {
  requestApprove,
  modifyApproveError,
};

export default {
  state,
  mutations,
};
