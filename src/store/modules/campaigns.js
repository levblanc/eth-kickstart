import _ from 'lodash';
import { setType } from '@/store/utils';
import {
  CAMPAIGN_LIST_REQUEST,
  CAMPAIGN_LIST_SUCCESS,
  CAMPAIGN_LIST_FAILURE,
} from '@/store/mutationTypes';
import factory from '$eth/factory';

const moduleName = 'campaignList';
const type = setType(moduleName);

const state = {
  loading: false,
  list   : [],
  error  : null,
};

const mutations = {
  [type(CAMPAIGN_LIST_REQUEST)](state) {
    state.loading = true;
  },
  [type(CAMPAIGN_LIST_SUCCESS)](state, res) {
    state.loading = false;
    state.list    = res;
    state.error   = null;
  },
  [type(CAMPAIGN_LIST_FAILURE)](state, err) {
    state.loading = false;
    state.list    = [];
    state.error   = err;
  },
};


const getCampaignList = async ({ commit }) => {
  commit(type(CAMPAIGN_LIST_REQUEST));

  try {
    const campaigns = await factory.methods.getDeployedCampaigns().call();
    commit(type(CAMPAIGN_LIST_SUCCESS), _.reverse(campaigns));
  } catch (err) {
    commit(type(CAMPAIGN_LIST_FAILURE), err);
  }
};


export const actions = {
  getCampaignList,
};

export default {
  state,
  mutations,
};
