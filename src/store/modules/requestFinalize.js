import { setType } from '@/store/utils';
import {
  REQUEST_FINALIZE_REQUEST,
  REQUEST_FINALIZE_SUCCESS,
  REQUEST_FINALIZE_FAILURE,
  MODIFY_ERROR_MSG,
} from '@/store/mutationTypes';
import Campaign from '$eth/campaign';
import web3 from '$eth/web3';

const moduleName = 'requestFinalize';
const type = setType(moduleName);

const state = {
  loading: false,
  success: false,
  error  : '',
};

const mutations = {
  [type(REQUEST_FINALIZE_REQUEST)](state) {
    state.loading = true;
  },
  [type(REQUEST_FINALIZE_SUCCESS)](state, res) {
    state.loading = false;
    state.success = res;
    state.error   = '';
  },
  [type(REQUEST_FINALIZE_FAILURE)](state, err) {
    state.loading = false;
    state.success = false;
    state.error   = err;
  },
  [type(MODIFY_ERROR_MSG)](state, msg) {
    state.error = msg;
  },
};


const requestFinalize = async ({ commit }, { address, index }) => {
  commit(type(REQUEST_FINALIZE_REQUEST));

  const campaign = Campaign(address);

  try {
    const accounts = await web3.eth.getAccounts();

    const res = await campaign.methods.finalizeRequest(index)
      .send({
        from: accounts[0],
        gas: '3000000',
      });

    commit(type(REQUEST_FINALIZE_SUCCESS), res);
  } catch (err) {
    commit(type(REQUEST_FINALIZE_FAILURE), err.message);
  }
};

const modifyFinalizeError = ({ commit }, msg) => {
  commit(type(MODIFY_ERROR_MSG), msg);
};


export const actions = {
  requestFinalize,
  modifyFinalizeError,
};

export default {
  state,
  mutations,
};
