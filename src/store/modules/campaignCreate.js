import { setType } from '@/store/utils';
import {
  CAMPAIGN_CREATE_REQUEST,
  CAMPAIGN_CREATE_SUCCESS,
  CAMPAIGN_CREATE_FAILURE,
  MODIFY_ERROR_MSG,
} from '@/store/mutationTypes';

import factory from '$eth/factory';
import web3 from '$eth/web3';

const moduleName = 'campaignCreate';
const type = setType(moduleName);

const state = {
  loading: false,
  address: '',
  error  : '',
};

const mutations = {
  [type(CAMPAIGN_CREATE_REQUEST)](state) {
    state.loading = true;
  },
  [type(CAMPAIGN_CREATE_SUCCESS)](state, address) {
    state.loading = false;
    state.address = address;
    state.error   = '';
  },
  [type(CAMPAIGN_CREATE_FAILURE)](state, err) {
    state.loading = false;
    state.address = '';
    state.error   = err;
  },
  [type(MODIFY_ERROR_MSG)](state, msg) {
    state.error = msg;
  },
};


const campaignCreate = async ({ commit }, {
  campaignName,
  campaignDesc,
  minimumContribution,
}) => {
  commit(type(CAMPAIGN_CREATE_REQUEST));

  try {
    const accounts = await web3.eth.getAccounts();

    factory.events.CampaignCreated()
      .on('data', ({ returnValues }) => {
        commit(
          type(CAMPAIGN_CREATE_SUCCESS),
          returnValues.newCampaignAddress,
        );
      })
      .on('error', (error) => {
        commit(type(CAMPAIGN_CREATE_FAILURE), error);
      });

    await factory.methods.createCampaign(
      campaignName,
      campaignDesc,
      web3.utils.toWei(minimumContribution, 'ether'),
    ).send({
      from: accounts[0],
      gas: '3000000',
    });
  } catch (error) {
    commit(type(CAMPAIGN_CREATE_FAILURE), error.message);
  }
};

const modifyNewCampaignError = ({ commit }, msg) => {
  commit(type(MODIFY_ERROR_MSG), msg);
};


export const actions = {
  campaignCreate,
  modifyNewCampaignError,
};

export default {
  state,
  mutations,
};
