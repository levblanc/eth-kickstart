import { setType } from '@/store/utils';
import {
  CAMPAIGN_MANAGER_REQUEST,
  CAMPAIGN_MANAGER_SUCCESS,
  CAMPAIGN_MANAGER_FAILURE,
  USER_ACCOUNT_UPDATE,
} from '@/store/mutationTypes';
import web3 from '$eth/web3';
import Campaign from '$eth/campaign';

const moduleName = 'campaignManager';
const type = setType(moduleName);

const state = {
  loading  : false,
  manager  : '',
  user     : '',
  isManager: false,
  error    : '',
};

const mutations = {
  [type(CAMPAIGN_MANAGER_REQUEST)](state) {
    state.loading = true;
  },
  [type(CAMPAIGN_MANAGER_SUCCESS)](state, { manager, user }) {
    state.loading   = false;
    state.manager   = manager.toLowerCase();
    state.user      = user.toLowerCase();
    state.isManager = state.user === state.manager;
    state.error     = '';
  },
  [type(CAMPAIGN_MANAGER_FAILURE)](state, err) {
    state.loading   = false;
    state.manager   = '';
    state.user      = '';
    state.isManager = false;
    state.error     = err;
  },
  [type(USER_ACCOUNT_UPDATE)](state, address) {
    state.user      = address.toLowerCase();
    state.isManager = state.user === state.manager;
  },
};


const getCampaignManager = async ({ commit }, address) => {
  commit(type(CAMPAIGN_MANAGER_REQUEST));

  const campaign = Campaign(address);

  web3.currentProvider.publicConfigStore.on('update', ({ selectedAddress }) => {
    commit(type(USER_ACCOUNT_UPDATE), selectedAddress);
  });

  try {
    const accounts = await web3.eth.getAccounts();
    const manager  = await campaign.methods.manager().call();

    commit(type(CAMPAIGN_MANAGER_SUCCESS), {
      manager,
      user: accounts[0],
    });
  } catch (err) {
    commit(type(CAMPAIGN_MANAGER_FAILURE), err.message);
  }
};

export const actions = {
  getCampaignManager,
};

export default {
  state,
  mutations,
};
