import { setType } from '@/store/utils';
import {
  CAMPAIGN_BALANCE_REQUEST,
  CAMPAIGN_BALANCE_SUCCESS,
  CAMPAIGN_BALANCE_FAILURE,
} from '@/store/mutationTypes';
import web3 from '$eth/web3';

const moduleName = 'campaignBalance';
const type = setType(moduleName);

const state = {
  loading: false,
  balance: 0,
  error  : '',
};

const mutations = {
  [type(CAMPAIGN_BALANCE_REQUEST)](state) {
    state.loading = true;
  },
  [type(CAMPAIGN_BALANCE_SUCCESS)](state, balance) {
    state.loading = false;
    state.balance = balance;
    state.error   = '';
  },
  [type(CAMPAIGN_BALANCE_FAILURE)](state, err) {
    state.loading = false;
    state.balance = 0;
    state.error   = err;
  },
};


const getCampaignBalance = async ({ commit }, address) => {
  commit(type(CAMPAIGN_BALANCE_REQUEST));

  try {
    const balance = await web3.eth.getBalance(address);

    commit(type(CAMPAIGN_BALANCE_SUCCESS), web3.utils.fromWei(balance, 'ether'));
  } catch (err) {
    commit(type(CAMPAIGN_BALANCE_FAILURE), err.message);
  }
};

export const actions = {
  getCampaignBalance,
};

export default {
  state,
  mutations,
};
