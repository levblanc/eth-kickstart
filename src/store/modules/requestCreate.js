import { setType } from '@/store/utils';
import {
  CREATE_REQUEST_REQUEST,
  CREATE_REQUEST_SUCCESS,
  CREATE_REQUEST_FAILURE,
  MODIFY_ERROR_MSG,
} from '@/store/mutationTypes';
import Campaign from '$eth/campaign';
import web3 from '$eth/web3';

const moduleName = 'requestCreate';
const type = setType(moduleName);

const state = {
  loading: false,
  success: false,
  error  : '',
};

const mutations = {
  [type(CREATE_REQUEST_REQUEST)](state) {
    state.loading = true;
  },
  [type(CREATE_REQUEST_SUCCESS)](state, res) {
    state.loading = false;
    state.success = res;
    state.error   = '';
  },
  [type(CREATE_REQUEST_FAILURE)](state, err) {
    state.loading = false;
    state.success = false;
    state.error   = err;
  },
  [type(MODIFY_ERROR_MSG)](state, msg) {
    state.error = msg;
  },
};


const requestCreate = async ({ commit }, {
  address,
  description,
  value,
  recipient,
}) => {
  commit(type(CREATE_REQUEST_REQUEST));

  const campaign = Campaign(address);

  try {
    const accounts = await web3.eth.getAccounts();

    const res = await campaign.methods.createRequest(
      description,
      web3.utils.toWei(value, 'ether'),
      recipient,
    ).send({
      from: accounts[0],
      gas: '3000000',
    });

    commit(type(CREATE_REQUEST_SUCCESS), res);
  } catch (err) {
    commit(type(CREATE_REQUEST_FAILURE), err.message);
  }
};

const modifyNewRequestError = ({ commit }, msg) => {
  commit(type(MODIFY_ERROR_MSG), msg);
};


export const actions = {
  requestCreate,
  modifyNewRequestError,
};

export default {
  state,
  mutations,
};
