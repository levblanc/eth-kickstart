import _ from 'lodash';
import { setType } from '@/store/utils';
import {
  REQUEST_LIST_REQUEST,
  REQUEST_LIST_SUCCESS,
  REQUEST_LIST_FAILURE,
} from '@/store/mutationTypes';
import Campaign from '$eth/campaign';

const moduleName = 'getRequestList';
const type = setType(moduleName);

const state = {
  loading       : false,
  list          : [],
  count         : 0,
  totalApprovers: 0,
  error         : '',
};

const mutations = {
  [type(REQUEST_LIST_REQUEST)](state) {
    state.loading = true;
  },
  [type(REQUEST_LIST_SUCCESS)](state, {
    requestList,
    requestCount,
    totalApprovers,
  }) {
    state.loading        = false;
    state.list           = requestList;
    state.count          = requestCount;
    state.totalApprovers = totalApprovers;
    state.error          = '';
  },
  [type(REQUEST_LIST_FAILURE)](state, err) {
    state.loading = false;
    state.error   = err;
  },
};


const getRequestList = async ({ commit }, address) => {
  commit(type(REQUEST_LIST_REQUEST));

  const campaign = Campaign(address);

  try {
    const requestCount   = await campaign.methods.getRequestsCount().call();
    const totalApprovers = await campaign.methods.approversCount().call();

    const requests = _.map(
      Array(parseInt(requestCount, 10)).fill(),
      async (item, index) => campaign.methods.requests(index).call(),
    );

    const requestList = await Promise.all(requests);

    commit(type(REQUEST_LIST_SUCCESS), {
      requestList,
      requestCount: parseInt(requestCount, 10),
      totalApprovers: parseInt(totalApprovers, 10),
    });
  } catch (err) {
    commit(type(REQUEST_LIST_FAILURE), err.message);
  }
};


export const actions = {
  getRequestList,
};

export default {
  state,
  mutations,
};
