import _ from 'lodash';
import { setType } from '@/store/utils';
import {
  SET_LANGUAGE,
  CHANGE_LANGUAGE,
} from '@/store/mutationTypes';
import lang from '@/lang';

const moduleName = 'language';
const type = setType(moduleName);

const initLangState = {};

_.each(lang, (value, key) => {
  initLangState[key] = {};
});

const state = {
  setting: 'cn',
  ...initLangState,
};

const mutations = {
  [type(CHANGE_LANGUAGE)](state) {
    state.setting = state.setting === 'cn' ? 'en' : 'cn';
  },
  [type(SET_LANGUAGE)](state) {
    _.each(lang, (value, key) => {
      state[key] = lang[key][state.setting];
    });
  },
};


const setLanguage = async ({ commit }) => {
  commit(type(SET_LANGUAGE));
};

const changeLanguage = async ({ commit }) => {
  commit(type(CHANGE_LANGUAGE));
  commit(type(SET_LANGUAGE));
};


export const actions = {
  setLanguage,
  changeLanguage,
};

export default {
  state,
  mutations,
};
