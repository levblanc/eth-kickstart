import { setType } from '@/store/utils';
import {
  CAMPAIGN_SUMMARY_REQUEST,
  CAMPAIGN_SUMMARY_SUCCESS,
  CAMPAIGN_SUMMARY_FAILURE,
} from '@/store/mutationTypes';

import Campaign from '$eth/campaign';
import web3 from '$eth/web3';

const moduleName = 'campaignSummary';
const type = setType(moduleName);

const state = {
  loading            : false,
  summary            : [],
  balance            : 0,
  minimumContribution: 0,
  error              : null,
};

const mutations = {
  [type(CAMPAIGN_SUMMARY_REQUEST)](state) {
    state.loading = true;
  },
  [type(CAMPAIGN_SUMMARY_SUCCESS)](state, {
    campaignSummary,
    campaignBalance,
    minimumContribution,
  }) {
    state.loading             = false;
    state.summary             = campaignSummary;
    state.balance             = campaignBalance;
    state.minimumContribution = minimumContribution;
    state.error               = null;
  },
  [type(CAMPAIGN_SUMMARY_FAILURE)](state, err) {
    state.loading             = false;
    state.summary             = [];
    state.balance             = 0;
    state.minimumContribution = 0;
    state.error               = err;
  },
};


const getCampaignSummary = async ({ commit }, address) => {
  commit(type(CAMPAIGN_SUMMARY_REQUEST));

  try {
    const campaign            = Campaign(address);
    const summary             = await campaign.methods.getSummary().call();
    const minimumContribution = web3.utils.fromWei(summary[0], 'ether');
    const campaignBalance     = web3.utils.fromWei(summary[1], 'ether');

    const campaignSummary = [
      {
        value: summary[5],
        meta: 'campaignName',
        description: '',
      },
      {
        value: summary[4],
        meta: 'campaignManager',
        description: 'managerDesc',
      },
      {
        value: summary[6],
        meta: 'campaignDesc',
        description: '',
      },
      {
        value: campaignBalance,
        meta: 'campaignBalance',
        description: 'balanceDesc',
      },
      {
        value: minimumContribution,
        meta: 'minContribution',
        description: 'minContributionDesc',
      },
      {
        value: summary[2],
        meta: 'requests',
        description: 'requestDesc',
      },
      {
        value: summary[3],
        meta: 'contributors',
        description: 'contributorDesc',
      },
    ];

    commit(type(CAMPAIGN_SUMMARY_SUCCESS), {
      campaignSummary,
      campaignBalance,
      minimumContribution,
    });
  } catch (err) {
    commit(type(CAMPAIGN_SUMMARY_FAILURE), err);
  }
};


export const actions = {
  getCampaignSummary,
};

export default {
  state,
  mutations,
};
